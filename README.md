# **Definition of Done for Merge Requests**

_Copy and paste this text to the description of your merge request._

## **Dev Kit**
- [ ] Tested in singleplayer
- [ ] Tested with hosted multiplayer with at least two players (both tested)
- [ ] Tested on dedicated server with at least two players (both tested)

## **Trello**
- [ ] Trello story updated and moved to "Ready for Review"
