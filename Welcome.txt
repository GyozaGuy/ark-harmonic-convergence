Hey! Welcome to the group!

Just a couple things to go over before you get on the server:

The rooles:
1. No swearing

2. Be civil

3. Avoid touchy subjects (religion, politics, etc.)

4. Do not suggest mods or changes to server settings to the admins

5. Listen to the Moderators and Administrators. Your respect is noticed and appreciated. Any disrespect to us or other players won't have much leniency.

5. Have fun and report any problems you find!

Important stuff to know:
1. The admins are all very busy people with very full lives (apologies if we can't help you right away!) and are developing AvatARK on the side. Your help is very much appreciated! Even if it's just playing with the mod.

2. Since we're still in major testing, expect crashes every once in a while.

3. The roles in Discord (Recruit, Regular, Veteran) are just roles to help gauge players activity in the community. It doesn't make you better than anyone else. We'll rank you up as we see your activity in the community continue. This is to help us delegate responsibilities and help others know who to turn to for help.

Choosing your Nation:
There are four nations to choose from:
Fire - Ruled by GyozaGuy
Water - Led by Xacheiree
Earth - Kinged by Siburnout
Air - Instructed by Whitii

Which nation you choose decides which bending power you will receive. Fire benders will be in the fire nation. Water benders in the water tribe, etc. No exceptions. 

Please let us know what your decision is as soon as possible! This helps us keep things moving along. We will give you the link and password to the server as soon as you decide.

Remember to invite your friends!